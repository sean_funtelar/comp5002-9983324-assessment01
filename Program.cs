﻿using System;

namespace comp5002_9983324_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {

/*
-START PROGRAM
-WELCOME USER
-DECLARE VARIABLES
-ASK USER TO INPUT NAME
-STORE CUSTOMER NAME AS A VARIABLE 
-INPUT PRODUCT 1ST QUANTITY(NUMBER)
-ASK USER IF THEY WANT TO ADD ANOTHER QUNATITY(NUMBER)
                           IF YES (TRUE)
                                          ASK USER TO INPUT 2ND QUANTITY (2ND NUMBER)
                                          ADD 1ST NUMBER TO 2ND NUMBER 
                                          DISPLAY TOTAL AMOUNT
                           IF NO (FALSE)
                                           DISPLAY TOTAL
-ADD GST TO TOTAL
-DISPLAY GST ADDED AMOUNT TO THE USER
-DISPLAY THANKS SCREEN
-STOP PROGRAM
 
 */

            //Declare variables

            //DISPLAYS TOTAL,GST,WITH GST(COULD BE DONE AS AN ALGORITHIM BUT IT WOULDNT DISPLAY ALL VARIABLES(SEE Reciept))
            
            double item1 = 0;
            double item2 = 0 ;
            double itemTotal = 0;
            double total = 0;
            double gst = 0.15;
            double totalGst = 0;
            
            //CORE VARIABLES 

            var name = ""; 
            var decision = "";
            

            
            Console.Clear();//How to clear CONSOLE/TERMINAL

            //*WELCOME SPLASH SCREEN FOR THE USER(SIMPLE TERMINAL GRAPHIC BY USING Console.Writeline to display characters hense ("")
             
            Console.WriteLine("[*****************************************]");
            Console.WriteLine("[0---------------------------------------0]");
            Console.WriteLine("[##########################################]");
            Console.WriteLine("[############ WELCOME TO THE ##############]");
            Console.WriteLine("[--------------KRUSTY KRAB-----------------]");
            Console.WriteLine("[##########################################]");
            Console.WriteLine("[##########################################]");
            Console.WriteLine("[##########################################]");
            Console.WriteLine("[##########################################]");
            Console.WriteLine("[---------PLEASE ENTER YOUR NAME :D--------]");
            Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS
            Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS

       
        name = (Console.ReadLine()); //THIS LETS USER INPUT ALPHANUMERIC CHARACTERS (NAME)

            //SPLASH SCREEN THAT DICTATES TO INPUT QUANTITY OF USERS 1ST PURCHASE (DECLARED item1)
            Console.WriteLine("[*******************************************]");
            Console.WriteLine("[0-----------------------------------------0]");
            Console.WriteLine("[###########################################]");
            Console.WriteLine($"[--------------Thanks  {name}-----------------]");
            Console.WriteLine("[-Please ENTER the PRICE of your FIRST item-]");
            Console.WriteLine("[-------------------------------------------]");
            Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS
            Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS

        item1 = double.Parse(Console.ReadLine());
            
            //INVALID CHARACTER WAS ENTERED RETURN TO SPLASH SCREEN 
            gotodecision:

            //SPLASH SCREEN THAT DICTATES TO ASK USER IF THEY WANT TO ADD ANOTHER ITEM DECLARED (ITEM2)
            
            Console.WriteLine("[----------------------------------------]");
            Console.WriteLine($"[-----------Hey there {name}-------------]");
            Console.WriteLine("[  Is there something else ? Add it here :D  ]");
            Console.WriteLine("[----------------------------------------]");
            Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS
            Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS
            
            Console.WriteLine("[****************************************]");
            Console.WriteLine("[          'Y' = Yes / 'N' = No          ]");
            Console.WriteLine("[****************************************]");
            Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS
            Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS

        decision = Console.ReadLine();//READS Y/N FOR decision
        if (decision == "Y")// IF STATEMENT IF CUSTOMER SAYS YES (Y)


            {
                //USER TO INPUT QUNATITY OF SECOND ITEM (ITEM2)
                Console.WriteLine("[*****************************************]");
                Console.WriteLine("[*****************************************]");
                Console.WriteLine("[   Enter the price of your second item   ]");
                Console.WriteLine("[*****************************************]");
                Console.WriteLine("[*****************************************]");
                Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS
                Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS
                
       item2 = double.Parse(Console.ReadLine());//USER INPUT OF SECOND QUANTITY DECLARED (ITEM2)

               
        itemTotal = item1+item2;//ITEM 1 AND 2 BIENG ADDED 

                //SPLASH SCREEN FOR decision YES (Y) SHOWING ITEM 1 AND 2 ADDED WITH GST 
                Console.WriteLine("[*************************]");
                Console.WriteLine("[*********Reciept*********]");
                Console.WriteLine("[*************************]");
                Console.WriteLine($"[.First Item-------${item1.ToString("F")}.]");//<< DECIMAL PLACEMENT CODE 
                Console.WriteLine($"[.Second Item------${item2.ToString("F")}.]");//<< DECIMAL PLACEMENT CODE 
                Console.WriteLine("[*************************]");
                Console.WriteLine($"[.Total Excl GST---${itemTotal.ToString("F")}.]");//<< DECIMAL PLACEMENT CODE 
                Console.WriteLine("[                         ]");
            }

            //If no
        else if (decision == "N")// IF STATEMENT WHEN CUSTOMERS SAYS NO 

            {
                
        itemTotal = item1;//ITEM 1 AND 2 BIENG ADDED

                //Display total
                Console.WriteLine("[*************************]");
                Console.WriteLine("[*********Reciept*********]");
                Console.WriteLine("[*************************]");
                Console.WriteLine($"[.First Item-------${item1.ToString("F")}.]");//<< DECIMAL PLACEMENT CODE 
                Console.WriteLine("[*************************]");
                Console.WriteLine($"[.Total Excl GST---${itemTotal.ToString("F")}.]");//<< DECIMAL PLACEMENT CODE 
                Console.WriteLine("[                         ]");//CREATES SPACING 
                
            }

       // ERROR CHECKING CODE (NOT NEEDED)
       /* else//If statement when entered an invalid cahracter 

            {
                Console.WriteLine("[----------------------------------------]");
                Console.WriteLine("[----------------------------------------]");
                Console.WriteLine("[  Invalid character! Please try again!  ]");
                Console.WriteLine("[----------------------------------------]");
                Console.WriteLine("[----------------------------------------]");
                Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS
                Console.WriteLine();//SPACING BETWEEN SPLASH SCREENS
                
        goto gotodecision;//RETURN LINE TO SPLASH SCREEN 

            }*/
            
            total = (itemTotal*gst)+itemTotal;//PRE DETERMINE GST QUANTITIY  MULTIPLIED WITH ITEM TOTAL
            totalGst = itemTotal*gst;//ITEM TOTAL MULTIPLIED WITH PREDETERMINE GST QUANTITIY 
            Console.WriteLine($"[.GST...............${totalGst.ToString("F")}.]");//<< DECIMAL PLACEMENT CODE 
            Console.WriteLine($"[.Total............${total.ToString("F")}.]");//<< DECIMAL PLACEMENT CODE 
            Console.WriteLine("[-------------------------]");
            Console.WriteLine("[ Thank you for shopping  ]");
            Console.WriteLine($"[       with us {name}      ]");
            Console.WriteLine("[    AAAAAAARRRRRRRRRRR   ]");
            Console.WriteLine("[-------------------------]");
            Console.WriteLine("[-------------------------]");

        }
    }
}